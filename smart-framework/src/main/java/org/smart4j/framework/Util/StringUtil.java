package org.smart4j.framework.Util;

import org.apache.commons.lang3.StringUtils;

public class StringUtil {
    /**
     * 判定字符串是否非空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
       if (str != null){
           str = str.trim();
       }
       return StringUtils.isNotEmpty(str);
    }

    /**
     * 判定字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return !isNotEmpty(str);
    }

    /**
     * 将字符串分隔为字符串数组
     * @param body
     * @param str
     * @return
     */
    public static String[] splitString(String body, String str) {
        return body.split(str);
    }
}
