package org.smart4j.framework.Util;

public final class CastUtil {
    /**
     * 转为long型，提供默认值0
     * @param object
     * @return
     */
    public static long castLong(Object object) {
        return CastUtil.castLong(object,0);
    }

    /**
     * 转为long型，可设置默认值
     * @param object
     * @param defaultValue
     * @return
     */
    public static long castLong(Object object, int defaultValue) {
        long longValue = defaultValue;
        if (object != null){
            String strValue = castString(object);
            if (StringUtil.isNotEmpty(strValue)){
                try{
                    longValue = Long.parseLong(strValue);
                }catch (NumberFormatException e){
                    longValue = defaultValue;
                }
            }
        }
        return longValue;
    }

    /**
     * 转为string类型，默认值为""
     * @param object
     * @return
     */
    public static String castString(Object object) {
        return castString(object,"");
    }

    /**
     * 转为string类型，可设置默认值
     * @param object
     * @return
     */
    public static String castString(Object object, String defaultValue) {
        return object == null ? defaultValue : String.valueOf(object);
    }
}
