package org.smart4j.framework.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropsUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropsUtil.class);

    /**
     * 加载属性文件
     * @param fileName
     * @return
     */
    public static Properties loadProps(String fileName) {
        Properties props = null;
        InputStream is = null;

        try {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            if (is == null){
                throw new FileNotFoundException(fileName+"文件不存在");
            }
        } catch (IOException e) {
            LOGGER.error("加载属性文件失败",e);
        }finally {
            if (is !=null){
                try {
                    is.close();
                } catch (IOException e) {
                    LOGGER.error("input stream关闭失败",e);
                }
            }
        }

        return props;
    }

    /**
     * 获取字符型属性（默认为空字符串）
     * @param props
     * @param key
     * @return
     */
    public static String getString(Properties props, String key) {
        return getString(props,key,"");
    }

    /**
     * 获取字符型属性，可指定默认值
     * @param props
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getString(Properties props, String key, String defaultValue) {
        String value = defaultValue;
        if (props.contains(key)){
            value=props.getProperty(key);
        }
        return value;
    }
}
