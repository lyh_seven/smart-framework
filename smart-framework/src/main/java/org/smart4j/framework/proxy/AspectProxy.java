package org.smart4j.framework.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * 切面代理模板类
 */
public abstract class AspectProxy implements Proxy{
    private static final Logger LOGGER = LoggerFactory.getLogger(AspectProxy.class);

    public final Object doProxy(ProxyChain proxyChain) throws Throwable {
        Object result = null;

        Class<?> cls = proxyChain.getTargetClass();
        Method method = proxyChain.getTargetMethod();
        Object[] params = proxyChain.getMethodParams();

        begin();
        try {
            if (intercept(cls,method,params)){
                before(cls,method,params);
                result = proxyChain.doProxyChain();
                after(cls,method,params);
            }else {
                result = proxyChain.doProxyChain();
            }
        }catch (Exception e){
            LOGGER.error("代理失败",e);
            error(cls,method,params,e);
        }finally {
            end();
        }
        return result;
    }

    private void end() {
    }

    private void error(Class<?> cls, Method method, Object[] params, Throwable e) {
    }

    private void after(Class<?> cls, Method method, Object[] params) throws Throwable{
    }

    private void before(Class<?> cls, Method method, Object[] params) throws Throwable{
    }

    private boolean intercept(Class<?> cls, Method method, Object[] params) throws Throwable{
        return true;
    }

    public void begin(){
    }
}
