package org.smart4j.framework.Helper;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.smart4j.framework.Annotation.Action;
import org.smart4j.framework.Annotation.Inject;
import org.smart4j.framework.Util.ArrayUtil;
import org.smart4j.framework.Util.CollectionUtil;

import org.smart4j.framework.bean.Handler;
import org.smart4j.framework.bean.Request;

/**
 * 控制器助手类
 */
public class ControllerHelper {
    /**
     * 用于存放request和handler的映射关系
     */
    private static final Map<Request, Handler> ACTION_MAP = new HashMap<Request, Handler>();

    static{
        Set<Class<?>> controllerClassSet = ClassHelper.getControllerClassSet();
        if (CollectionUtil.isNotEmpty(controllerClassSet)){
            for (Class<?> controllerClass : controllerClassSet){
                //获取类中定义的方法
                Method[] methods = controllerClass.getDeclaredMethods();
                if (ArrayUtil.isNotEmpty(methods)){
                    for (Method method : methods){
                        //判断当前方法是否有Action注解
                        if(method.isAnnotationPresent(Action.class)){
                            //从action注解中获取url映射规则
                            Action action = method.getAnnotation(Action.class);
                            String mapping = action.value();

                            //验证url映射规则
                            if (mapping.matches("\\w+:/\\w*")){
                                String[] array = mapping.split(".");
                                if (ArrayUtil.isNotEmpty(array) && array.length==2){
                                    String requestMethod = array[0];
                                    String requestPath = array[1];
                                    Request request = new Request(requestMethod,requestPath);
                                    Handler handler = new Handler(controllerClass,method);

                                    //存于ActionMap中
                                    ACTION_MAP.put(request,handler);
                                }
                            }

                        }
                    }
                }
            }

        }
    }

    /**
     * 获取handler
     * @param requestMethod
     * @param requestPath
     * @return
     */
    public static Handler getHandler(String requestMethod, String requestPath){
        Request request = new Request(requestMethod,requestPath);
        return ACTION_MAP.get(request);
    }
}
