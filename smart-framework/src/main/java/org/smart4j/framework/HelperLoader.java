package org.smart4j.framework;

import org.smart4j.framework.Helper.AopHelper;
import org.smart4j.framework.Helper.BeanHelper;
import org.smart4j.framework.Helper.ClassHelper;
import org.smart4j.framework.Helper.ControllerHelper;
import org.smart4j.framework.Helper.IocHelper;
import org.smart4j.framework.Util.ClassUtil;

/**
 * 加载相应的Helper类
 */
public class HelperLoader {
    //实际上第一次访问类的时候，就会加载其static块，这里只是为了加载更加集中，所以写在了这里集中加载

    public static void init(){
        //特别注意：AopHelper要在IocHelper之前加载
        Class<?>[] classList = new Class[]{
                ClassHelper.class,
                BeanHelper.class,
                AopHelper.class,
                IocHelper.class,
                ControllerHelper.class
        };

        for (Class<?> cls: classList){
            ClassUtil.loadClass(cls.getName(),true);
        }
    }

}
