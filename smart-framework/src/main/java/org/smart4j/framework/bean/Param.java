package org.smart4j.framework.bean;

import java.util.Map;

import org.smart4j.framework.Util.CastUtil;
import org.smart4j.framework.Util.CollectionUtil;

public class Param {
    private Map<String,Object> paramMap;

    public Param(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

    /**
     * 根据参数名获取long类型的参数值
     * @param name
     * @return
     */
    public long getLong(String name){
        return CastUtil.castLong(paramMap.get(name));
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    /**
     * 验证参数是否为空
     * @return
     */
    public boolean isEmpty() {
        return CollectionUtil.isEmpty(paramMap);
    }
}
