package org.smart4j.framework.bean;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 封装请求信息
 */
public class Request {
    //请求方法
    private String requestMehod;
    //请求路径
    private String requestPath;

    public Request(String requestMehod, String requestPath) {
        this.requestMehod = requestMehod;
        this.requestPath = requestPath;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this,o);
    }

    @Override
    public int hashCode() {
       return HashCodeBuilder.reflectionHashCode(this);
    }

    public String getRequestMehod() {
        return requestMehod;
    }


    public String getRequestPath() {
        return requestPath;
    }

}
